import React from 'react';
import NewsItem from '../News/NewsItem';
import newsData from '../newsData';

const Games = () => {
  const categoryNews = newsData
    .filter((item) => item.category === 'Games')
    .map((item) => {
      return <NewsItem key={item.id} title={item.title} date={item.date} />;
    });

  return <div className="news-games">{categoryNews}</div>;
};

export default Games;
